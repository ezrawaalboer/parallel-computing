(ns bucket.bucketsort)

(defn bucket-nums
  [width start xs]
  (loop [buckets []
         stream xs
         threshold (+ start width)]
    (println buckets)

    (if (empty? stream)
      buckets
      (let [[under beyond] (split-with #(< % threshold) stream)]
        (recur (conj buckets under)
               beyond
               (+ threshold width))))))

(println (bucket-nums 1000 0 [1 3045 456 2005 35 3987]))