(defn insertion-sort [xs]
  (letfn [(insert [col x]
            (loop [[y & ys] col acc []]
              (cond
                (nil? y) (conj acc x)
                (<= x y) (vec (concat acc [x y] ys))
                :else (recur ys (conj acc y)))))]
    (reduce insert [] xs)))

(defn bucket-sort
  "Runs in O(n) time."
  [items]
  (let [len (count items)
        mx (apply max items)
        bucket-size (inc (int (/ mx len)))
        buckets (reduce (fn [v n] (conj v [])) []
                        (range (+ bucket-size (/ mx bucket-size))))]
    (letfn [(distrib-nums [v n]
              (let [ind (int (/ n bucket-size))
                    bucket (nth v ind)]
                (assoc! v ind (conj bucket n))))]
      (let [pre-buckets (persistent!
                          (reduce distrib-nums (transient buckets) items))]
        (apply concat (map (fn [bucket]
                             (when (> (count bucket) 0)
                               (sort bucket))) pre-buckets))))))





(defn bucket-sort-parallel
  "Runs in O(n) time."
  [items]
  ;; "let" binds variables, these variables only exist within the let param.
  ;; Lenght = the amount of variables given
  ;; maxValue = the greates value within the array
  ;; BucketSize = the size of the bucket
  ;; bucketare the buckets/
  (let [lenght (count items)
        maxValue (apply max items)
        bucket-size (inc (int (/ maxValue lenght)))
        buckets (reduce (fn [v n] (conj v [])) []
                        (range (+ bucket-size (/ maxValue bucket-size))))]
    (println "lenght" lenght)
    (println "maxValue" maxValue)
    (println "bucketSize" bucket-size)
    (println "BucketCount" (count buckets))

    (letfn [(distrib-nums [v n]
              (let [ind (int (/ n bucket-size))
                    bucket (nth v ind)]
                (assoc! v ind (conj bucket n))))]
      (let [pre-buckets (persistent!
                          (reduce distrib-nums (transient buckets) items))]
        (apply concat (map (fn [bucket]
                             (when (> (count bucket) 0)
                               (sort bucket))) pre-buckets))))
    ))

;; Build an array of a given amount numbers
(let [cnt 1e6
      rnums (map (fn [n] (int (rand cnt))) (range cnt))]

  ;;;; warmup
  ;(doseq [c (range 1)]
  ;  (take 10 (bucket-sort-parallel rnums))
  ;  (take 10 (sort rnums)))
  ;
  ;(doseq [c (range 10)]
  ;      (future (take 10 (bucket-sort rnums)))
  ;      (future (take 10 (sort rnums))))

  ;; run
  (println (time (take 10 (bucket-sort rnums))))
  (println (time (take 10 (sort rnums)))))
