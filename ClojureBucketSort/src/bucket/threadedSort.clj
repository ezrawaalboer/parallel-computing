(ns bucket.threadedSort)

; Define variables
(def thread-count 2
  )                                        ; the amount of threads that can be used
(def array-size 1e4)                                      ; the size of the array

(def rn-array (pmap (fn [n] (int (rand array-size))) (range array-size))); Create random array

(def buckets (into [] (repeat array-size [])))


(def split-rn-array (partition-all (/ array-size thread-count) rn-array))

(defn bucket-sort [xs]
  (let [bucket-count array-size
        add-item     (fn [buckets item]
                       (update-in buckets
                                  [item]
                                  #(conj % item)))]
    (reduce add-item buckets xs)
    ))

(defn do-bucket-sort [index
                      array]
  (println "Started thread " index)
  (def buckets (bucket-sort (nth array index)) )
  (println "")
  )

(println (count split-rn-array))

(defn do-loop [array]

  (dotimes [x (count array)]
    (println "This is loop : " x)

      (.start (Thread (do-bucket-sort x array)))
    )
  (apply concat buckets)
  )

(println "Bucket Sort")
(println (time (take 10 (do-loop split-rn-array))))

;(println "Normal Sort")
;(println (time (take 10 (sort rn-array))))

(println "Finished")
