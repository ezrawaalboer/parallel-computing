(ns clojure-rmi.core)

; to simplify
(import java.net.InetAddress)
(import java.rmi.registry.LocateRegistry)


(def port 1199)

; fully qualified
(def localHost
  (java.net.InetAddress/getLocalHost)
  )

(println "Host:"(.getHostName localHost))
(println "Adress:"(.getHostAddress localHost))
(println "Fully qualified:"(.getCanonicalHostName localHost))

(def myreg (java.rmi.registry.LocateRegistry/createRegistry 2000))

(def reg (java.rmi.registry.LocateRegistry/getRegistry (.getHostName localHost) port))


; protocols must have at least one argument
; http://clojure.org/reference/protocols
(defprotocol MyService (ping [x]))



