(ns counter.core)


;; create an atom holding my-counts
;(def my-counts (atom 0) )
;
;; counter increases my-counts 'counts' times
;(defn counter [counts]
;  (
;    doseq [x (range counts)] ; loop x from 0 .. counts-1
;    (
;      swap! my-counts inc ; increase my-count
;            )
;    )
;  ;(Thread/sleep 10000)
;  )
;

;; spawn threads and do some parallel counting
;(println my-counts)
;(dotimes [_ 10000] ; spawn 10000 threads !
;  (.start (Thread. (counter 100)))
;  )
;(println my-counts)
;

; create an ref holding my-counts
(def my-counts (ref 0) )

; counter increases my-counts 'counts' times
(defn counter [counts]
  (
    doseq [x (range counts)] ; loop x from 0 .. counts-1
    (
      dosync (
               alter my-counts inc ; increase my-count
                     ))
    )
  ;(Thread/sleep 10000)
  )


; spawn threads and do some parallel counting
(println my-counts)
(dotimes [_ 5] ; spawn 10000 threads !
  (.start (Thread. (counter 100)))
  (println "Hello")
  )
(println my-counts)



;; create an agent holding my-counts
;(def my-counts (agent 0) )
;
;; counter increases my-counts 'counts' times
;(defn counter [counts]
;  (
;    doseq [x (range counts)] ; loop x from 0 .. counts-1
;    (
;      send my-counts inc ; increase my-count
;           )
;    )
;  ;(Thread/sleep 10000)
;  )
;
;
;; spawn threads and do some parallel counting
;(println my-counts)
;(dotimes [_ 10000] ; spawn 10000 threads !
;  (.start (Thread. (counter 100)))
;  )
;(println my-counts)

