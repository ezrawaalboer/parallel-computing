(ns hello-world.core)

; rko 28/6/2016
; start an newly created thread with an anonymous (clojure) function
; note: clojure functions are runnable by default
(.start (Thread. #(println "Hello from new thread 1")))
(.start (Thread. #(println "Hello from new thread 2")))



(Thread/sleep 2000) ; delay

(println "Hello from main thread")
