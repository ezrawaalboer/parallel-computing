(ns map-and-reduce.core
  (:require [clojure.core.reducers :as r])
  )



; http://yellerapp.com/posts/2014-12-11-14-race-condition-in-clojure-println.html
(defn safe-println [& more]
  (.write *out* (str (clojure.string/join " " more) "\n")))


; set a timestamp
(defn restart-profiling []
  (def last-time-stamp (atom (System/nanoTime))
    )
  )

; get ms since last timestamp
(defn get-delta-ms []
  (let [last @last-time-stamp
        current (System/nanoTime)
        ticks (- current last)
        ]

    (restart-profiling)
    (float (/ ticks 1000000)) ; calculate the delta in milliseconds
    )
  )

; inspired by https://clojuredocs.org/clojure.core/pmap
; range is het aantal getallen
(def some-numbers (range 10))

(restart-profiling)
(map inc some-numbers)
(safe-println "Mapping a fast function "(get-delta-ms) "ms")

(defn slow-inc [x]
  (Thread/sleep 10)
  (inc x) ; calculate inc x
  )

(restart-profiling)
(map slow-inc some-numbers)
(safe-println "Mapping a slow function "(get-delta-ms) "ms")

; pmap is a parrallel mao
(restart-profiling)
(pmap inc some-numbers)
(safe-println "Parallel Mapping a fast function "(get-delta-ms) "ms")

(restart-profiling)
(pmap slow-inc some-numbers)
(safe-println "Parallel Mapping a slow function "(get-delta-ms) "ms")


; combining pmaps for partitions and maps for serial calculations
(restart-profiling)
(count
  (apply concat
         (pmap (fn [partition] (doall (map slow-inc partition)))
               (partition-all 1e1 (range 1e3))
               )
         ))
(safe-println "Parallel Mapping a slow (map) function onto a partition "(get-delta-ms) "ms")


(restart-profiling)
(count (map slow-inc (range 1e3)))
(safe-println "Serial Mapping a slow (map) function onto an unpartitioned same size dataset "(get-delta-ms) "ms")


; reduction
(defn slow+
  ([] (Thread/sleep 1) 0)
  ([x y] (Thread/sleep 1) (+ x y))
  )


(restart-profiling)
(reduce + some-numbers)
(safe-println "Reducing a fast function "(get-delta-ms) "ms")

(restart-profiling)
(reduce slow+ some-numbers)
(safe-println "Reducing a slow function "(get-delta-ms) "ms")


(restart-profiling)
(r/fold slow+ some-numbers)
(safe-println "Reducing a slow function with parallel fold"(get-delta-ms) "ms")

; http://clojure.org/reference/reducers
(safe-println "Oooops not faster -> fold splits dataset in 512 elements  before performance gain can be achieved")
(safe-println)


; from https://adambard.com/blog/clojure-reducers-for-mortals/
(def more-numbers (range 1e4))

(restart-profiling)
(reduce + more-numbers)
(safe-println "Reducing a fast function "(get-delta-ms) "ms")

(restart-profiling)
(r/fold + more-numbers)
(safe-println "Reducing with parallel fold"(get-delta-ms) "ms")


(safe-println)
(def waaay-more-numbers (range 1e6))

(restart-profiling)
(reduce + waaay-more-numbers)
(safe-println "Reducing a fast function "(get-delta-ms) "ms")

(restart-profiling)
(r/fold + waaay-more-numbers)
(safe-println "Reducing with parallel fold"(get-delta-ms) "ms")

(safe-println "Weird fold doesnt seem to improve performance much!")

; full benchmark
; from https://adambard.com/blog/clojure-reducers-for-mortals/

(safe-println)
(defn benchmark [f N times]
  (let [nums (vec (range N))
        start (java.lang.System/currentTimeMillis)]
    (dotimes [n times]
      (f nums))
    (- (java.lang.System/currentTimeMillis) start)))


(def N 1e6)
(def times 10)


(defn old-map-reduce [nums]
  (reduce + (map inc (map inc (map inc nums)))))

(defn new-map [nums]
  (reduce + (r/map inc (r/map inc (r/map inc nums)))))

(defn new-reduce [nums]
  (r/fold + (map inc (map inc (map inc nums)))))

(defn new-map-reduce [nums]
  (r/fold + (r/map inc (r/map inc (r/map inc nums)))))

(println "Old map reduce: " (benchmark old-map-reduce N times) "ms")
(println "New map: " (benchmark new-map N times) "ms")
(println "New reduce: " (benchmark new-reduce N times) "ms")
(println "New map reduce:   " (benchmark new-map-reduce N times) "ms")







