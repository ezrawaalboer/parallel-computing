(ns parallel-min.core
  (:import (java.util.concurrent Executor Executors)))



; http://yellerapp.com/posts/2014-12-11-14-race-condition-in-clojure-println.html
(defn safe-println [& more]
  (.write *out* (str (clojure.string/join " " more) "\n")))


; set a timestamp
(defn restart-profiling []
  (def last-time-stamp (atom (System/nanoTime))
    )
  )

; get ms since last timestamp
(defn get-delta-ms []
  (let [last @last-time-stamp
        current (System/nanoTime)
        ticks (- current last)
        ]

    (restart-profiling)
    (float (/ ticks 1000000)) ; calculate the delta in milliseconds
    )
  )


; start the profiling
(restart-profiling)

(defn generate-test
  [size]
  (shuffle(range -10 (- size 10)))
  )
(safe-println (get-delta-ms) "ms")


;; small-test
(def small-test (generate-test 1e1))
(safe-println (count small-test) " elements in small-test")
(safe-println (get-delta-ms) "ms")
(safe-println "Minimum" (apply min small-test))
(safe-println (get-delta-ms) "ms")


;; huge-test
(def huge-test
  (shuffle (generate-test 1e6))
  )
(safe-println (count huge-test) " elements in huge-test")
(safe-println (get-delta-ms) "ms")
(safe-println "Minimum" (apply min huge-test))
(safe-println (get-delta-ms) "ms")

(def cores (.availableProcessors (Runtime/getRuntime)))
(safe-println "Available cores " cores)

;;
(def parallel-test small-test)

;; partition the input into parts, for example as many parts as there are cores
(def parts (group-by #(mod % cores) parallel-test))
(safe-println "Original vector " parallel-test)
(safe-println "Split into ")
(dotimes
  [x cores]
  (safe-println (parts x))
  )


; explicit function to calculate the minimum of a partition x
(defn min-of-part
  [x]
  (apply min(parts x))
  )

; mapping explicit function on 0..cores-1
(def
  all-mins
  (reduce min (map min-of-part (range cores)))
  )

(safe-println "Minimum " all-mins)


;; bit more thinking :) mapping an anonymous function to calculated the minimum per partion over all partitions
(def
  all-mins-anonymous
  (reduce min (map #(apply min(parts %)) (range cores)))
  )

(safe-println "Minimum " all-mins-anonymous)

; now add threading

; first define some thread pools to play with
(def fixed-pool-1 (Executors/newFixedThreadPool 1))
(def fixed-pool-cores (Executors/newFixedThreadPool cores))
(def cached-pool (Executors/newCachedThreadPool))

; a experiment with spawning tasks in a threadpool
; http://blog.raek.se/2011/01/24/executors-in-clojure/

(defn sleep-print [x]
  (Thread/sleep 100)
  (println x "done!") ; use 'unsafe' printing
  x)

(let [tasks (for [i (range 10)]
              #(sleep-print i))
      futures (.invokeAll fixed-pool-1 tasks)]
  (for [ftr futures]
    (.get ftr)))

(.shutdown fixed-pool-1)


;; now mess up the layout in printing
(let [tasks (for [i (range 10)]
              #(sleep-print i))
      futures (.invokeAll cached-pool tasks)]
  (for [ftr futures]
    (.get ftr)))

(.shutdown cached-pool)


;; now do the same for spawning tasks that calculate minimum on a partition of the vector
(dotimes [_ 3] (safe-println))
(restart-profiling)
(def cached-pool (Executors/newCachedThreadPool))

(def parallel-mins
  (let [tasks (for [i (range cores)]   ; the tasks are the partitions
                  #(min-of-part i))    ; that we calculate minimum on
        futures (.invokeAll cached-pool tasks)] ; just the same

    (for [ftr futures]
      (.get ftr))))

(println parallel-mins)
(.shutdown cached-pool)

; the partitions are hidden in the solution above ... make it explicit

(dotimes [_ 3] (safe-println))
(restart-profiling)
(def cached-pool (Executors/newCachedThreadPool))

(def global-parallel-min
  (let [
        partitions (group-by #(mod % cores) parallel-test)  ; calculate 'core' partitions

        tasks (for [i (range cores)]                        ; create 'core' tasks
                #(apply min (partitions i)))                ; that we calculate minimum on

        futures (.invokeAll cached-pool tasks)              ; invoke the tasks on the threadpool

        result (for [ftr futures]                           ; collect results in a sequence
                 (.get ftr))
        ]

    (apply min result)  ; calculate the 'global' minimum for the sequence
    )
  )

(println global-parallel-min)
(.shutdown cached-pool)


; generify the solution to a function for a collection and the function manages its own threadpool
(defn parallel-min [col]
  (let [
        pool (Executors/newCachedThreadPool)
        partitions (group-by #(mod % cores) col)  ; calculate 'core' partitions

        tasks (for [i (range cores)]                        ; create 'core' tasks
                #(apply min (partitions i)))                ; that we calculate minimum on

        futures (.invokeAll pool tasks)              ; invoke the tasks on the threadpool

        minimums (for [ftr futures]                           ; collect results in a sequence
                 (.get ftr))
        ]

      (.shutdown pool)
      (apply min minimums)        ; calculate the 'global' minimum for the sequence to return
    )
  )

(safe-println "Minimum" (parallel-min parallel-test))

(restart-profiling)
(safe-println "Parallel minimum on Huge test" (parallel-min huge-test) " takes " (get-delta-ms) "ms")

(restart-profiling)
(safe-println "Minimum" (apply min huge-test) " takes "(get-delta-ms) "ms")
(safe-println "OOOOOOOPPPS :(")

; what is the reason for this slow parallel computation??

; benchmark partitioning
(defn partition-test [c]
  (group-by #(mod % cores) c)
  )

(restart-profiling)
(safe-println "Partitioning " (count (partition-test huge-test)) " takes "(get-delta-ms) "ms")


; faster partition using subvec O(1)
; https://clojuredocs.org/clojure.core/subvec
(defn my-partition
  [c p]
  (let [
        task-size (quot (count c) cores)
        start (* p task-size)
        end (* (inc p) task-size)
        ]
    (subvec c start end)
    )
  )

(defn faster-parallel-min [col]
  (let [
        pool (Executors/newCachedThreadPool)

        tasks (for [i (range cores)]                  ; create 'core' tasks
                #(apply min (my-partition col i)))    ; that we calculate minimum on

        futures (.invokeAll pool tasks)               ; invoke the tasks on the threadpool

        minimums (for [ftr futures]                   ; collect results in a sequence
                   (.get ftr))
        ]

    (.shutdown pool)
    (apply min minimums)        ; calculate the 'global' minimum for the sequence to return
    )
  )

(restart-profiling)
(safe-println "Faster Parallel minimum on Huge test" (faster-parallel-min huge-test) " takes " (get-delta-ms) "ms")

