(ns producer-consumer-async.core
  (:require [clojure.core.async :as async :refer :all
             :exclude [map into reduce merge partition partition-by take]]))

(use 'clojure.java.io)

; inspired by
; http://elbenshira.com/blog/using-core-async-for-producer-consumer-workflows/
; http://stackoverflow.com/questions/7756909/in-clojure-1-3-how-to-read-and-write-a-file


; define a text-file
(def text-file "test-producer-consumer.txt")


; create a text file with 100 lines and 1.21 dots on each line
(with-open [wrtr (writer text-file)]
  (dotimes [n 100]
    (.write wrtr
            (apply str (repeat (+ 1 (rand-int 20)) "."))
            )
    (.newLine wrtr )
    )
  )


; a process normal does some work on a line of text, and that takes computing time (sleep)
(defn process
  "Do 'work'"
  [line]
  (Thread/sleep 100)
  line)

; check if it processes each line
(with-open [rdr (reader text-file)]
  (doseq [line (line-seq rdr)]
    (process line)
    (println line)))


; now time it without printing each line, just time the processing time
(time (with-open [rdr (reader text-file)]
        (doseq [line (line-seq rdr)]
          (process line)
          )))



; now use channels to print all lines in a textfile
; textfile -> read-file -> file-chan -> print-chan -> console

(def file-chan (async/chan))
(defn read-file
  "read lines from a file and put them in a file-channel"
  []
  (async/thread
    (with-open [rdr (reader text-file)]
      (doseq [line (line-seq rdr)]
        (async/>!! file-chan line)
        ))
    )
  )

(defn print-chan
  "print lines from the file-channel to the console"
  []
  (async/thread
    (while true
      (let [data (async/<!! file-chan)]
        (println data)
        )
      )
    )
  )

(do
  (print-chan)
  (read-file)
  )

;; generify print-chan to print from a channel

(defn print-from-chan
  "print lines from the file-channel to the console"
  [ch]
  (async/thread
    (while true
      (let [data (async/<!! ch)]
        (println data)
        )
      )
    )
  )

(do
  (print-from-chan file-chan)
  (read-file)
  )


; textfile -> read-file -> file-chan -> chan-consumer [n] -> out-chan -> print-from-chan -> console
(def out-chan (async/chan))

;; now create a chan-consumer
(defn chan-consumer
  "starts n consumers that process elements from a channel and puts it in another channel"
  [n]
  (dotimes [_ n]
    (async/thread
      (while true
        (let [line (async/<!! file-chan)
              data (process line)]
          (async/>!! out-chan data)
          )
        )
      )
    )
  )


(do
  (print-from-chan out-chan)
  (chan-consumer 8)
  (read-file)
  )
