(ns producer-consumer-async.core
  (:require [clojure.core.async :as async :refer :all
             :exclude [map into reduce merge partition partition-by take]]))

(use 'clojure.java.io)

; inspired by
; http://elbenshira.com/blog/using-core-async-for-producer-consumer-workflows/
; http://stackoverflow.com/questions/7756909/in-clojure-1-3-how-to-read-and-write-a-file


; define a text-file
(def text-file "test-producer-consumer.txt")


; create a text file with 100 lines and 1.21 dots on each line
(with-open [wrtr (writer text-file)]
  (dotimes [n 100]
    (.write wrtr
            (apply str (repeat (+ 1 (rand-int 20)) "."))
            )
    (.newLine wrtr )
    )
  )


; a process normal does some work on a line of text, and that takes computing time (sleep)
(defn process
  "Do 'work'"
  [line]
  (Thread/sleep 100))

; check if it processes each line
(with-open [rdr (reader text-file)]
  (doseq [line (line-seq rdr)]
  (process line)
  (println line)))


; now time it without printing each line, just time the processing time
(time (with-open [rdr (reader text-file)]
    (doseq [line (line-seq rdr)]
      (process line)
      )))

; now use channels to print all lines in a textfile
; textfile -> chan-reader -> chan -> chan-printer -> console
(def c (async/chan))

(with-open [rdr (reader text-file)]
  (doseq [line (line-seq rdr)]
    (process line)
    (println line)))


(defn chan-reader
  "Read items from file and store in chan"
  []
  (thread
    (with-open [rdr (reader text-file)]
      (doseq [line (line-seq rdr)]
        (>!! c line)
        )
      )
    )
  )

(defn chan-printer
  "Take items from the chan and print it."
  []
  (thread
    (while true
      (let [line (<!! c)]
        (println line)
        )
      )
    )
  )


(do
  (chan-reader)
  (chan-printer)
  )


; now use channels to print all lines in a textfile
; textfile -> in-chan -> out-chan -> println
;  (def in-chan (aync/chan))
;  (def out-chan (aync/chan))


