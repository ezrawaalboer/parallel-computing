(ns producer-consumer-async.test
  (:require [clojure.core.async :as async :refer :all
             :exclude [map into reduce merge partition partition-by take]]))

(use 'clojure.java.io)

(def thread-count 4)
(def array-size 100)                                        ; the size of the array

(def random-array (pmap (fn [n] (int (rand array-size))) (range array-size))) ; Create random array

(defn bucket-sort [xs]
  (let [bucket-count array-size
        buckets (into [] (repeat bucket-count []))
        add-item (fn [buckets item]
                   (update-in buckets
                              [item]
                              #(conj % item)))]
    (reduce add-item buckets xs)
    ))


(def in-chan (async/chan))
(def out-chan (async/chan))

(defn start-async-consumers
  [num-consumers]
  (dotimes [_ num-consumers]
    (async/thread
      (let [array (async/<!! in-chan)
            data (bucket-sort array)]
        (async/>!! out-chan data)))))


; Producer splits the array in multiple items and puts those in In-chan
(defn start-async-producer
  [num-producers]
  (dotimes [_ 1]
    (async/thread

      (let [split-array (partition-all (/ array-size thread-count) random-array)]

        (dotimes [x (count split-array)]
          (async/>!! in-chan (nth split-array x))
          )
        ))))

; The Main Bucket where all buckets from the consumers will be put into
(def mainbuckets (into [] (repeat array-size [])))


(defn update-bucket [test]
  (def mainbuckets test)
  )

; Method to merge the data from the consumors into the main bucket
(defn mergeBuckets [xs]
  (let [add-item (fn [mainbuckets index]
                   (let [input #(into % (nth xs index))]
                     (update-in mainbuckets
                                [index]
                                input
                                )
                     )

                   )]
    (dotimes [x (count mainbuckets)]
      (update-bucket (add-item mainbuckets x))
      )
    ))




(defn do-loop []

  (while (if (< (count (apply concat mainbuckets)) array-size) true false)
    (let [data (async/<!! out-chan)]
      (mergeBuckets data)
      ))
  )

(do

  (start-async-producer 1)
  (start-async-consumers thread-count)

  (println (time (take 10 (do-loop))))
  (println (take 10 mainbuckets))
  (println "Finished")
  )
