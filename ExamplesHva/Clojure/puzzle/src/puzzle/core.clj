(ns puzzle.core)



;; initial version
;(def answer-ready (atom false))
;(def answer (atom 0))
;;
;(.start (Thread.
;          #(
;            do
;            (reset! answer-ready true)
;            (Thread/sleep 1000) ;; slow down this thread
;            (reset! answer 42)
;            nil
;            )))
;
;(.start (Thread. (
;           #(if (true? @answer-ready)
;             (println "The meaning of life is" @answer)
;             (println "I don't know the answer")
;             )
;           )))



;; improved version ... synchronize both atoms -> ooops

;(def answer-ready (atom false))
;(def answer (atom 0))
;
;(.start (Thread.
;          #(dosync
;            (reset! answer-ready true)
;            (Thread/sleep 1000) ;; slow down this thread
;            (reset! answer 42)
;            nil)
;          ))
;
;
;(.start (Thread. (
;                   #(if (true? @answer-ready)
;                     (println "The meaning of life is" @answer)
;                     (println "I don't know the answer")
;                     )
;                   )))
;
;(Thread/sleep 1500) ; wait until other thread is done
;
;(.start (Thread. (
;                   #(if (true? @answer-ready)
;                     (println "The meaning of life is" @answer)
;                     (println "I don't know the answer")
;                     )
;                   )))


;; the correct version: in order to use coordinated changes between threads we need refs
;(def answer-ready (ref false))
;(def answer (ref 0))
;
;(.start (Thread.
;          #(dosync
;            (ref-set answer-ready true)
;            (Thread/sleep 1000) ;; slow down this thread
;            (ref-set answer 42)
;            nil)
;          ))
;
;
;(.start (Thread. (
;                   #(if (true? @answer-ready)
;                     (println "The meaning of life is" @answer)
;                     (println "I don't know the answer")
;                     )
;                   )))
;
;(Thread/sleep 1500) ; wait until other thread is done
;
;(.start (Thread. (
;                   #(if (true? @answer-ready)
;                     (println "The meaning of life is" @answer)
;                     (println "I don't know the answer")
;                     )
;                   )))







