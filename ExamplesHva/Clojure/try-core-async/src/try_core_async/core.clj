(ns try-core-async.core
  (:require [clojure.core.async :as async :refer :all
             :exclude [map into reduce merge partition partition-by take]]))


; inspired by
; https://github.com/clojure/core.async/blob/master/examples/walkthrough.clj
; chapter 6, seven concurrency models in seven weeks
; https://clojure.github.io/core.async/

; a chan is an unbuffered channel
(def c (chan))

; write to a channel with >!!
; read from a channel with <!!

; by default channels are synchronous/unbuffered
; so if we would write to a channel the IDE will freeze, until we read from the channel
; thats why we spawn a read thread first which unblocks when we write to it
(thread (println "Reading from a thread:" (<!! c)))
(>!! c "Hello channel")

; the other way around
(thread (>!! c "Writing hello to channel") (println "After write is done"))
(<!! c)


; buffered channels, will block if full on write/empty on read
(def bc (chan 5))
(>!! bc 1)
(>!! bc 2)
(<!! bc)
(<!! bc)

; read everything from a channel into a vector
(defn readall!! [ch]
  (loop [coll []]
    (if-let [x (<!! ch)]
      (recur (conj coll x))
      coll)))

; write everything from a vector to a channel
(defn writeall!! [ch coll]
  (doseq [x coll]
    (>!! ch x))
  (close! ch))

(def ch (chan 100))

(writeall!! ch (range 0 10))

; dropping buffers/sliding buffers
(def dc (chan (dropping-buffer 5)))
(onto-chan dc (range 0 10))
(<!! (async/into [] dc))

(def sc (chan (sliding-buffer 5)))
(onto-chan sc (range 0 10))
(<!! (async/into [] sc))


; go blocks are transformed into a non-blocking statemachine, parking when
; waiting on input/output
(go
  (let [x (<! c)]
    (println x)
    )
  )

(>!! c "Hallo from outside go block")

; multiple channels

(let [c1 (chan)
      c2 (chan)]
  (thread (while true
            (let [[v ch] (alts!! [c1 c2])]
              (println "Read" v "from" ch))))
  (>!! c1 "hi")
  (>!! c2 "there"))


(let [c1 (chan)
      c2 (chan)]
  (go (while true
            (let [[v ch] (alts! [c1 c2])]
              (println "Read" v "from" ch))))
  (>!! c1 "hi")
  (>!! c2 "there"))


(let [n 1000
      cs (repeatedly n chan)
      begin (System/currentTimeMillis)]
  (doseq [c cs] (go (>! c "hi")))
  (dotimes [i n]
    (let [[v c] (alts!! cs)]
      (assert (= "hi" v))))
  (println "Read" n "msgs in" (- (System/currentTimeMillis) begin) "ms"))








