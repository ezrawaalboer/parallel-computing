(ns word-count.core
  (:require [clojure.core.reducers :as r])
  )


; http://yellerapp.com/posts/2014-12-11-14-race-condition-in-clojure-println.html
(defn safe-println [& more]
  (.write *out* (str (clojure.string/join " " more) "\n")))


; set a timestamp
(defn restart-profiling []
  (def last-time-stamp (atom (System/nanoTime))
    )
  )

; get ms since last timestamp
(defn get-delta-ms []
  (let [last @last-time-stamp
        current (System/nanoTime)
        ticks (- current last)
        ]

    (restart-profiling)
    (float (/ ticks 1000000)) ; calculate the delta in milliseconds
    )
  )


;
;; word count
;
(def small-dictionary ["purple", "orange", "silver",  "people", "family"])

(defn generate-test
  [col n]
  (shuffle (mapcat #(repeat n %) col))
  )

(def tiny-test (generate-test small-dictionary 1))
(def small-test (generate-test small-dictionary 10))
(def big-test (generate-test small-dictionary 1e6))


(restart-profiling)
(frequencies tiny-test)
(safe-println "Counting words with built in function" (get-delta-ms))

(defn count-words [coll]
  (r/fold
    (partial merge-with +)
    (fn [counts x] (assoc counts x (inc (get counts x 0))))
    coll))

(restart-profiling)
(count-words tiny-test)
(safe-println "Counting words with folding" (get-delta-ms))


; now partition the dataset

; each part contains 10 words
(def parts (partition-all 100 big-test))

(defn count-words-sequential [pages]
  (apply merge-with + (map frequencies pages)))

(defn count-words-parallel-map [pages]
  (reduce (partial merge-with +)
          (pmap frequencies pages)))

(defn count-words-parallel-map-fold [pages]
  (r/fold (partial merge-with +)
          (pmap frequencies pages)))



(restart-profiling)
(count-words-sequential parts)
(safe-println "Counting words with built in function over partitions" (get-delta-ms))

(restart-profiling)
(count-words-parallel-map parts)
(safe-println "Counting words with pmap over partitions" (get-delta-ms))

(restart-profiling)
(count-words-parallel-map-fold parts)
(safe-println "Counting words with pmap/fold over partitions" (get-delta-ms))
