import java.util.concurrent.BlockingQueue;

/**
 * Created by Laptop_Ezra on 5-1-2017.
 */
public class Consumer implements Runnable {

    private final BlockingQueue<int[]> sharedQueue;
    private int[] buckets;
    private int threadId;

    public Consumer(int maxValue, int threadId) {
        this.sharedQueue = Main.getQueue();
        this.buckets = new int[maxValue];
        this.threadId = threadId;
    }

    @Override
    public void run() {

        int[] inputArray;

        while (Main.getProducerStatus() || sharedQueue.size() > 0) {
            try {

                for (int i = 0; i < this.buckets.length; i++) {
                    this.buckets[i] = 0;
                }

                inputArray = sharedQueue.take();

                for (int i = 0; i < inputArray.length; i++) {
                    int test = inputArray[i];
                    buckets[test]++;
                }

                Main.updateBucket(buckets, this.threadId, inputArray);

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }


    }
}
