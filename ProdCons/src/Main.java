import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.concurrent.*;

public class Main {

    //Creating shared object
    private static BlockingQueue<int[]> sharedQueue = new LinkedBlockingQueue<>();

    public static boolean producerBusy;
    private static final int testProcessors = 8;
    private static final int dataSetSize = 1000000;
    private static final int maxValue = 10;
    private static int processorsAvailable;
    private static int[] dataSet;
    private static int[] mainBucket;
//    private static ArrayList<Integer> resultSet;
    private static int[] resultSet;
    private static EventProfiler profiler;
    private static ExecutorService executorConsumers;
    private static ExecutorService executorProducers;

    public static void main(String[] args) {
        profiler = new EventProfiler(true);
        profiler.start();

        createAndSortArray();
        detectAvailableProcessors();
        profiler.log("Setting up main variables");

        initExecService();
        profiler.log("shutdown Executor Service");

        sortMainBucket();
        profiler.log("Sorting Main bucket");

        System.out.println("Finished");
        System.out.println("Before:  " + dataSet.length);
        System.out.println("After:  " + resultSet.length);

    }


    private static boolean initExecService() {

        startProducer();
        startConsumers();
        profiler.log("Init Executor Service");

        executorProducers.shutdown();
        executorConsumers.shutdown();


        try {
            executorProducers.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            System.out.println("Execption in awaiting Termination");
            e.printStackTrace();
        }

        try {
            executorConsumers.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
            System.out.println("Execption in awaiting Termination");
            e.printStackTrace();
        }
        profiler.log("BucketSort Takes");

        return true;

    }

    private static void startConsumers(){
        executorConsumers = Executors.newFixedThreadPool(testProcessors);
        Consumer[] consumerArray = new Consumer[processorsAvailable];
        for (int i = 0; i < processorsAvailable; i++) {
            consumerArray[i] = new Consumer(maxValue, i);
        }
        for (Consumer consumer : consumerArray) {
            executorConsumers.execute(consumer);
        }
    }

    private static void startProducer(){
        executorProducers = Executors.newFixedThreadPool(1);
        Producer producer = new Producer(processorsAvailable, dataSet);
        executorProducers.execute(producer);
    }


    private static void createAndSortArray() {
        producerBusy = true;
        dataSet = Utils.createRandomArray(dataSetSize, maxValue);
        profiler.log("Filling array");
//        resultSet = new ArrayList<>();
        mainBucket = new int[maxValue];
        resultSet = new int[dataSetSize];
    }

    private static void detectAvailableProcessors() {
        processorsAvailable = Utils.getAvailableProcessors(testProcessors);
    }

    private static void sortMainBucket() {
        int outPos = 0;
        for (int i = 0; i < mainBucket.length - 1; i++) {
            for (int j = 0; j < mainBucket[i]; j++) {
                resultSet[outPos++] = i;
            }
        }
    }

//    private static void sortMainBucket() {
//        for (int i = 0; i < mainBucket.length; i++) {
//            for (int j = 0; j < mainBucket[i]; j++) {
//                resultSet.add(i);
//            }
//        }
//    }


    public synchronized static void updateBucket(int[] runBuckets, int id, int[] inputArray) {
        for (int i = 0; i < runBuckets.length; i++) {
            mainBucket[i] = mainBucket[i] + runBuckets[i];
        }
    }

    public synchronized static BlockingQueue getQueue(){
        return sharedQueue;
    }

    public synchronized static Boolean getProducerStatus(){
        return producerBusy;
    }

}
