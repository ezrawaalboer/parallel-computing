package Interfaces;

import Utils.Task;

import java.rmi.Remote;
import java.rmi.RemoteException;

/**
 * Created by Laptop_Ezra on 6-1-2017.
 */
public interface ServerInterface extends Remote {
    void ping() throws RemoteException;

    <T> Task getTask() throws RemoteException;
    <T> boolean sendResultTask(int[] result) throws RemoteException;

}
