package Interfaces;

/**
 * Created by Laptop_Ezra on 6-1-2017.
 */
public interface TaskInterface {
    int[] execute();
}
