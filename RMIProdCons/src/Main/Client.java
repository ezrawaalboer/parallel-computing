package Main;

import Interfaces.ServerInterface;
import Utils.Task;

import java.net.InetAddress;
import java.net.UnknownHostException;
import java.rmi.RemoteException;
import java.util.concurrent.TimeUnit;

/**
 * Created by Laptop_Ezra on 6-1-2017.
 */
public class Client {

    private static ServerInterface service;
    private static String localHostname;
    private static Task task;
    private static int[] taskResult;

    // run some methods/tasks
    private static long t1, t2;

    public static void main(String[] args) throws RemoteException, UnknownHostException, InterruptedException {

        connectClient();

//        sendPing();

        while (true){
            TimeUnit.MILLISECONDS.sleep(2);

            getTask();

            if(task != null){
                doTask();

                sendTaskResultToServer();
            }

        }



    }

    private static void getTask() throws RemoteException {
        System.out.println("Get Task");
        t1 = System.currentTimeMillis();
        task = service.getTask();
        t2 = System.currentTimeMillis();
        System.out.println("Get Task " + (t2-t1) + " ms.");
    }

    private static void doTask() {
        t1 = System.currentTimeMillis();
        taskResult = task.execute();
        t2 = System.currentTimeMillis();
        System.out.println("Task execution took " + (t2-t1) + " ms.");
        task = null;
    }

    private static void sendTaskResultToServer() throws RemoteException {
        t1 = System.currentTimeMillis();
        boolean result = service.sendResultTask(taskResult);
        t2 = System.currentTimeMillis();
        System.out.println("Sending task result took " + (t2-t1) + " ms.");
        taskResult = null;
    }


    private static void connectClient() throws UnknownHostException {
        localHostname = InetAddress.getLocalHost().getHostName();
        System.out.println("This is host:" + localHostname );

        // connect to the remote service host
        String serviceHost = Server.DefaultMasterNodeName;
//        if (args.length > 0)
//            serviceHost = args[0];
        service = Server.connect(serviceHost);
    }

    private static void sendPing() throws RemoteException {
        t1 = System.currentTimeMillis();
        service.ping();
        t2 = System.currentTimeMillis();
        System.out.println("Ping took " + (t2-t1) + " ms.");
    }
}
