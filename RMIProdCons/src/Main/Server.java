package Main;

import Interfaces.ServerInterface;
import PCs.Producer;
import Utils.EventProfiler;
import Utils.Task;
import Utils.Util;

import java.net.InetAddress;
import java.rmi.RemoteException;
import java.rmi.registry.LocateRegistry;
import java.rmi.registry.Registry;
import java.rmi.server.UnicastRemoteObject;
import java.util.concurrent.*;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 * Created by Laptop_Ezra on 6-1-2017.
 */
public class Server extends UnicastRemoteObject implements ServerInterface {

    /**** attributes needed for bucketSort ***********************/
    public static boolean producerBusy;
    public static final int maxValue = 10;
    public static int chunksCreated = 0;
    private static boolean acceptTasks = false;

    public static BlockingQueue<int[]> sharedQueue = new LinkedBlockingQueue<>();
    private static final int dataSetSize = 1000000;
    private static int tasksHandled = 0;

    private static final int testProcessors = 2;
    private static int processorsAvailable;

    private static int[] dataSet;
    private static int[] mainBucket;
    private static int[] resultSet;

    private static EventProfiler profiler;
    private static ExecutorService executorProducers;


    /**** attributes needed for server setup ***********************/
    public static final String DefaultMasterNodeName = "Pc_van_Ezra";
    public static final String ServiceName = "TaskServer";
    public static final int DefaultPort = 1199;

    private static String localHostname;

    /********************************************************/

    public Server() throws RemoteException {
    }

    public static void main(String[] args) throws InterruptedException {
        profiler = new EventProfiler(true);
        profiler.start();

        // Create Main.Server at localhost
        createRegistry();

        //Create And SortArray
        createAndSortArray();
        //Detect the available processors
//        System.out.println("Detect available processors");
        detectAvailableProcessors();
        profiler.log("Setup");

        //Split the array in portions
        initProducerService();
        profiler.log("Init Producers");

        TimeUnit.SECONDS.sleep(10);
        acceptTasks = true;
        profiler.log("Setup Time for clients");

        while(sharedQueue.size() > 0 || tasksHandled != chunksCreated){
            TimeUnit.MILLISECONDS.sleep(2);
        }
        profiler.log("BucketSort");

        sortMainBucket();
        profiler.log("Sort main Bucket");
        System.out.println("Finished");
        System.out.println("Before:  " + dataSet.length);
        System.out.println("After:  " + resultSet.length);
//        countZero(resultSet);
//        loopArray(resultSet);
    }

    public static void countZero(int[] array){

//        System.out.println("-----------  Count Zero array ----------");
        int count = 0;
        for(int i = 0; i < array.length; i++){
            if(array[i] == 0){
                count++;
            }
//            System.out.println(array[i]);
        }
//        System.out.println("The zero count : " + count);
    }

    public static void loopArray(int[] array){

        System.out.println("-----------  Loop array ----------");
        int count = 0;
        for(int i = 0; i < array.length; i++){
            System.out.println(array[i]);
        }
    }

    private static void createRegistry() {
        try {
            // get the hostname of this node
            localHostname = InetAddress.getLocalHost().getHostName();

            // start a new server object
            Server server = new Server();

            // start the registry service on this node
            Registry registry = LocateRegistry.createRegistry(Server.DefaultPort);

            // add binding to this server object and use a specific ServiceName to reference it
            registry.bind(Server.ServiceName, server);

            System.out.println(Server.ServiceName + " running on " + localHostname);

        } catch (Exception ex) {
            Logger.getLogger(Server.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }

    private static void createAndSortArray() {
        producerBusy = true;
        dataSet = Util.createRandomArray(dataSetSize, maxValue);
//        profiler.log("Filling array");
        mainBucket = new int[maxValue+1];
        resultSet = new int[dataSetSize];
    }

    private static void detectAvailableProcessors() {
        processorsAvailable = Util.getAvailableProcessors(testProcessors);
    }

    private static void sortMainBucket() {
        int outPos = 0;
        for (int i = 0; i < mainBucket.length - 1; i++) {
            for (int j = 0; j < mainBucket[i]; j++) {
                resultSet[outPos++] = i;
            }
        }
    }

    private static boolean initProducerService() {

        startProducer();
//        profiler.log("Init Executor Interfaces");
        executorProducers.shutdown();

        try {
            executorProducers.awaitTermination(1, TimeUnit.DAYS);
        } catch (InterruptedException e) {
//            System.out.println("Execption in awaiting Termination");
            e.printStackTrace();
        }
//        profiler.log("BucketSort Takes");
        return true;

    }

    private static void startProducer() {
        executorProducers = Executors.newFixedThreadPool(1);
        Producer producer = new Producer(processorsAvailable, dataSet);
        executorProducers.execute(producer);
    }

    public synchronized static void updateBucket(int[] runBuckets) {
        for (int i = 0; i < runBuckets.length; i++) {
            mainBucket[i] = mainBucket[i] + runBuckets[i];
        }
    }


    public static ServerInterface connect(String host) {

        ServerInterface remoteService = null;

        try {
            System.out.println("Connecting to " + host);

            Registry r = LocateRegistry.getRegistry(host, DefaultPort);
            remoteService = (ServerInterface) r.lookup(ServiceName);

        } catch (Exception ex) {
            Logger.getLogger(Client.class.getName()).log(Level.SEVERE, null, ex);
        }

        return remoteService;
    }

    public void ping() throws RemoteException {
        return;
    }

    @Override
    public Task getTask() throws RemoteException {

        if(acceptTasks){
            //        System.out.println("SERVER: Get Task Called");
            try {
//            System.out.println("SharedQueue Size: " + sharedQueue.size());
                int[] chunk = sharedQueue.take();
                Task task = new Task(chunk);
//            System.out.println("SERVER: Task Created");
                return task;

            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }

//        System.out.println("SERVER: Return NULL");
        return null;
    }

    @Override
    public boolean sendResultTask(int[] result) throws RemoteException {
        updateBucket(result);

//        countZero(result);

        tasksHandled++;
        return true;
    }

    public synchronized static BlockingQueue getQueue(){
        return sharedQueue;
    }


}
