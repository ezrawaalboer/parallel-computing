package PCs;

import java.util.Arrays;
import java.util.concurrent.BlockingQueue;
import Main.*;

/**
 * Created by Laptop_Ezra on 5-1-2017.
 */
public class Producer implements Runnable {

    private final BlockingQueue sharedQueue;
    private final int amountOfChunks;
    private int[] dataSet;

    public Producer(int amountOfChunks, int[] dataSet) {
        this.sharedQueue = Server.sharedQueue;
        this.amountOfChunks = amountOfChunks;
        this.dataSet = dataSet;
    }

    @Override
    public void run() {

        int splitSize = dataSet.length / amountOfChunks;
        int start = 0;
        int end = splitSize;
        int count = 0;
        for (int i = 0; i < amountOfChunks; i++) {
            int[] chunk = Arrays.copyOfRange(dataSet, start, end);

            start = end;
            end = end + splitSize;
            count++;
            if (end > dataSet.length || count == amountOfChunks-1) {
                end = dataSet.length;
            }

//            Server.countZero(chunk);

            sharedQueue.add(chunk);
            Server.chunksCreated++;
        }
//        System.out.println("Producer is Done");
        Server.producerBusy = false;

    }
}
