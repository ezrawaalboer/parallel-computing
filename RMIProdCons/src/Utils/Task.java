package Utils;

import Interfaces.TaskInterface;
import Main.*;

import java.io.Serializable;
import java.util.concurrent.BlockingQueue;

/**
 * Created by Laptop_Ezra on 6-1-2017.
 */
public class Task implements TaskInterface, Serializable {

    private int[] chunk;
    private int[] buckets;

    public Task(int[] chunk) {
        this.chunk = chunk;
        this.buckets = new int[Server.maxValue+1];
    }

    @Override
    public int[] execute() {

        int[] inputArray;

        for (int i = 0; i < this.buckets.length; i++) {
            this.buckets[i] = 0;
        }

        inputArray = chunk;

        for (int i = 0; i < inputArray.length; i++) {
            int test = inputArray[i];
            buckets[test]++;
        }

//        Main.updateBucket(buckets, this.threadId, inputArray);

//        System.out.println("Slow task done");
        return buckets;
    }
}