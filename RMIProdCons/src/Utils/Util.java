package Utils;

/**
 * Created by Laptop_Ezra on 5-1-2017.
 */
public class Util {

    public static int[] createRandomArray (int size, int maxValue){

        int[] array = new int[size];

        for (int i = 0; i < size; i++){
            array[i] = (int)(Math.random() * maxValue);
        }
        System.out.println("Created RandomArray, lenght: " + array.length);

        return array;

    }

    public static int getAvailableProcessors(int testProcessors){

        int processors = testProcessors;

        if(processors == 0){
            processors = Runtime.getRuntime().availableProcessors();
        }

        System.out.println("Processors " + processors);
        return processors;
    }
}

