import java.util.Arrays;

/**
 * Created by Laptop_Ezra on 29-11-2016.
 */
public class BucketThread extends Thread {

    private int[] buckets;
    private int[] inputArray;
    private int maximumValue;
    private int threadId;


    public BucketThread(int[] inputArray, int maxValue, int threadId) {
        this.maximumValue = maxValue;
        this.buckets = new int[maxValue];
        this.inputArray = inputArray;
        this.threadId = threadId;
//            System.out.println("Thread " + this.threadId + " , inputLenght : " + this.inputArray.length);

    }

    public void run(){

        for(int i = 0; i < this.buckets.length; i++){
            this.buckets[i] = 0;
        }

        for (int i=0; i< inputArray.length; i++) {
            int test = inputArray[i];
            buckets[test]++;
        }
//        if(this.threadId == 0){
//            for (int i=0; i< buckets.length; i++) {
//                System.out.println("Thread : " + this.threadId + ", bucket = " + i + ", amount = " + buckets[i]);
//            }
//        }


        Main.updateBucket(buckets, this.threadId, inputArray);
//        System.out.println("Test Thread");
    }


}
