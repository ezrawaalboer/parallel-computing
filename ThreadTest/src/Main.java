import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Main {

    private static final int testProcessors = 1;
    private static final int dataSetSize = 1000000;
    private static final int maxValue = 100000;
    private static int processorsAvailable;
    private static int[] dataSet;
    private static int[] mainBucket;
    private static int[] resultSet;


    public static void main(String[] args) throws InterruptedException {

        EventProfiler profiler = new EventProfiler(true);
        profiler.start();

        dataSet = Utils.createRandomArray(dataSetSize, maxValue);
        profiler.log("Filling array");
        resultSet = new int[dataSetSize];
        mainBucket = new int[maxValue];
        processorsAvailable = Utils.getAvailableProcessors(testProcessors);
        List<BucketThread> threads = new ArrayList<>();
        profiler.log("Setting up main variables");

        int splitSize = dataSet.length / processorsAvailable;
        int fromPos = 0;
        int toPos = splitSize;

        for(int i = 0; i < processorsAvailable; i++){
            int[] chunk = Arrays.copyOfRange(dataSet, fromPos, toPos);

            fromPos = toPos;
            toPos = toPos + splitSize;
            if(toPos > dataSet.length){
                toPos = dataSet.length;
            }

            threads.add(i, new BucketThread(chunk, maxValue, i));
            threads.get(i).start();
        }
        profiler.log("Splitting Array & Starting threads");

        for(int i = 0; i < threads.size(); i++){
                threads.get(i).join();
        }

        profiler.log("Joining Threads");

        int outPos=0;
        for (int i=0; i< mainBucket.length; i++) {
            for (int j=0; j< mainBucket[i]; j++) {
                resultSet[outPos++]=i;
            }
        }
        profiler.log("Sorting Main bucket");
        System.out.println("Before:  " + dataSet.length);
        System.out.println("After:  " + resultSet.length);
        System.out.println("Finished");
    }

    public synchronized static void updateBucket(int[] runBuckets, int id, int[] inputArray){
        for(int i = 0; i < runBuckets.length; i++){
            mainBucket[i] = mainBucket[i] + runBuckets[i];
        }
    }

}
